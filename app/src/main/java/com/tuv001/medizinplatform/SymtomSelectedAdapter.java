package com.tuv001.medizinplatform;

/**
 * Created by JTJ on 01.05.2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import java.util.ArrayList;


public class SymtomSelectedAdapter extends BaseAdapter
{

    private Context activity;
    private ArrayList<Symtom> data;
    private static LayoutInflater inflater = null;
    private View vi;
    private SymtomSelectedAdapter.ViewHolder viewHolder;

    public SymtomSelectedAdapter(Context context, ArrayList<Symtom> symtoms) {
        this.activity = context;
        this.data = symtoms;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        vi = view;
        //Populate the Listview
        final int pos = position;
        Symtom symtoms = data.get(pos);
        if(view == null) {
            vi = inflater.inflate(R.layout.listview_symtom, null);
            viewHolder = new ViewHolder();
            viewHolder.checkBox = (CheckBox) vi.findViewById(R.id.checkboxsymtom);
            viewHolder.name = (TextView) vi.findViewById(R.id.name);
            vi.setTag(viewHolder);
        }else
            viewHolder = (SymtomSelectedAdapter.ViewHolder) view.getTag();
        viewHolder.name.setText(symtoms.getSymtom());
        if(symtoms.isCheckbox()){
            viewHolder.checkBox.setChecked(true);
        }
        else {
            viewHolder.checkBox.setChecked(false);
        }
        return vi;
    }
    public ArrayList<Symtom> getAllData(){
        return data;
    }
    public void setCheckBox(int position){
        //Update status of checkbox
        Symtom symtoms = data.get(position);
        symtoms.setCheckbox(!symtoms.isCheckbox());
        notifyDataSetChanged();
    }

    public class ViewHolder{
        TextView name;
        CheckBox checkBox;
    }
}
