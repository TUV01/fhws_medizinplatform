package com.tuv001.medizinplatform;
/**
 * Created by JTJ
 */

//GUI (JTJ)
import android.content.Intent;//Import class Intent .To redirect to another activity.
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button; //Import class Button
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;//Import class Toast. A toast is a view containing a quick little message for the user.
//DB (Magommed)
import java.sql.Connection;
import java.sql.Statement;


public class Register extends AppCompatActivity {

    private Button buttonlogin2; //Object Button for Login.
    private Button buttonregister2; //Object Button for Register.
    private EditText etusername;//EditText username.
    private EditText etpassword;//EditText password.
    private EditText etemail;//EditText email.
    private EditText etfirstname;//EditText first name.
    private EditText etlastname;//EditText last name.
    protected String enteredusernameR; //To save the new username value entered by the user.
    protected String enteredpasswordR; //To save the new password value entered by the user.
    protected String enteredemailR; //To save the new email value entered by the user.
    protected String enteredfirstnameR;//To save the new first name entered by the user.
    protected String enteredlastnameR;//To save the new last name entered by the user.
    private CheckBox checkBoxshowPwd;//To show the password if the user wants.


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //EditText
        etusername=(EditText)findViewById(R.id.etUserIDNewAccount);
        etpassword=(EditText)findViewById(R.id.etPasswordUserNewAccount);
        etemail=(EditText)findViewById(R.id.editTextEmailR);
        etfirstname =(EditText)findViewById(R.id.etfirstnameR);
        etlastname =(EditText)findViewById(R.id.etlastnameR);


        //Show password
        checkBoxshowPwd=(CheckBox)findViewById(R.id.checkboxShow);
        etpassword=(EditText)findViewById(R.id.etPasswordUserNewAccount);
        etpassword.setTransformationMethod(new PasswordTransformationMethod());
        checkBoxshowPwd.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if (isChecked) {
                    etpassword.setTransformationMethod(null); // Show password when box checked
                } else {
                    etpassword.setTransformationMethod(new PasswordTransformationMethod()); // Hide password when box not checked
                }
            }
        } );

        //Button Login
        buttonlogin2=(Button)findViewById(R.id.buttonreturnlogin);//Button login correspond with buttonreturnLogin from activity_register.xml

        buttonlogin2.setOnClickListener(new View.OnClickListener() { //If click on the button login
            @Override
            public void onClick(View v) {
                Intent login=new Intent(Register.this,Login.class);// Redirect from Register to Login
                startActivity(login); //Start
            }
        });


        //Button Register
        buttonregister2=(Button)findViewById(R.id.bRegister); //Button register correspond with id.buttonRegister from activity_register.xml

        buttonregister2.setOnClickListener(new View.OnClickListener() { //If click on the button Register
            @Override
            public void onClick(View v) {

                enteredusernameR = etusername.getText().toString();
                enteredpasswordR = etpassword.getText().toString();
                enteredemailR = etemail.getText().toString();
                enteredfirstnameR = etfirstname.getText().toString();
                enteredlastnameR = etlastname.getText().toString();

                //Messages:
                if (enteredusernameR.equals("") || enteredpasswordR.equals("") || enteredemailR.equals("") || enteredfirstnameR.equals("") || enteredlastnameR.equals("")) {
                    Toast.makeText(Register.this, getResources().getString(R.string.messagemustfil), Toast.LENGTH_LONG).show();
                    //Error msg if username,password,email,first name or last name is equals ""
                    return;
                }
                if(enteredusernameR.length()<=5||enteredpasswordR.length()<=5)
                {
                    Toast.makeText(Register.this,R.string.messagefivecharacters,Toast.LENGTH_LONG).show();
                    //Error msg if username or password is less than five Characters
                    return;
                }

                //Else created by Magommed and JTJ(message and Intent)
                else {
                    Connection con;
                    String z = "";
                    try {
                        ConnectionDB cDB = new ConnectionDB();
                        con = cDB.connectDB(); // Connect to database
                        if (con == null) {
                            z = "Check Your Internet Access!";
                        } else {
                            String query = "insert into patienten (status,username, password, email, firstName, lastName) values("
                                    + "'" + " " + "'" + ","+ "'" + enteredusernameR + "'" + "," + "'" + enteredpasswordR + "'" + "," + "'" + enteredemailR + "'" + "," + "'" + enteredfirstnameR + "'" + "," + "'" + enteredlastnameR + "'" + ")";
                            Statement stmt = con.createStatement();
                            stmt.executeUpdate(query);

                            //GUI (JTJ) From *
                            // message and Intent
                            Toast.makeText(Register.this, getResources().getString(R.string.thanksregistering), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Register.this, Login.class);
                            startActivity(intent);
                            //to *

                            stmt.close();
                            con.close();

                        }


                    } catch (Exception ex) {

                        z = ex.getMessage();
                    }

                }
            }


            });

    }
}


