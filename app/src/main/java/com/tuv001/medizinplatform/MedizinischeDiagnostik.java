package com.tuv001.medizinplatform;
/**
 * Created by JTJ
 */
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MedizinischeDiagnostik extends AppCompatActivity {


    private Diagnostik patientHandler;
    private DiagnostikAdapter adapter;
    private ArrayList<Diagnostik> diagnostiksList; //DB Query Save here
    private ListView listView;
    private Button buttonfeedback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medizinische_diagnostik);
        listView=(ListView)findViewById(R.id.listview);
        setWidget();

        buttonfeedback= (Button) findViewById(R.id.gotoFeedback);
        buttonfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent=new Intent(MedizinischeDiagnostik.this,Feedback.class);
                startActivity(intent);
            }
        });
    }




    private void setWidget()
    {
        //Recovering the username from SharedPreferences
        SharedPreferences prefs = getSharedPreferences("Mypreferences", Context.MODE_PRIVATE);
        final String UsernameShared=prefs.getString("username","forexampleusername"); //username

        //Adding data to ArrayList
        diagnostiksList = new ArrayList<Diagnostik>();

        //DB Query from Username---> disease and medicine
        //String disease;
        //Diagnostik(String Disease,String Medicine);


        String disease="Grippe";
        diagnostiksList.add(new Diagnostik(gettranslation(disease),"Paracetamol")); //Just to try

        //Setup Adapter for Listview
        adapter = new DiagnostikAdapter(MedizinischeDiagnostik.this, diagnostiksList);
        listView.setAdapter(adapter);

    }

     //Method that translates disease into Spanish or English
     private String gettranslation(String disease)
     {
         Map<String,String> english=new HashMap<String, String>();
         Map<String,String> spanish=new HashMap<String, String>();
         english.put("Lebensmittelvergiftung","Food poisoning"); spanish.put("Lebensmittelvergiftung","Intoxicación Alimentaria");
         english.put("Grippe","Flu");spanish.put("Grippe","Gripe");
         english.put("Erkältung","Cold");spanish.put("Erkältung","Resfriado");
         english.put("Migräne","Migraine");spanish.put("Migräne","Migraña");
         english.put("Gicht","Gout");spanish.put("Gicht","Gota");
         english.put("Blinddarmentzündung","Appendicitis");spanish.put("Blinddarmentzündung","Apendicitis");
         english.put("Mandelentzündung","Amygdalitis");spanish.put("Mandelentzündung","Amigdalitis");
         english.put("Heuschnupfen","Hay fever");spanish.put("Heuschnupfen","Fiebre del heno");
         english.put("Lungenentzündung","Pneumonia");spanish.put("Lungenentzündung","Neumonía");
         english.put("Mittelohrentzündung","Otitis");spanish.put("Mittelohrentzündung","Otitis");

         //Get the current language
         String language= Locale.getDefault().getLanguage();

         if(language.equals("es"))
         {
            disease=spanish.get(disease);
         }
         else if(language.equals("en"))
         {
            disease=english.get(disease);
         }
        return disease;
     }
}

