package com.tuv001.medizinplatform;

/**
 * Created by JTJ-PC on 01.05.2017.
 */

public class Bewertung
{
    String medikament;
    String wert;

    public Bewertung(String medikament,String wert) {
        this.medikament = medikament;
        this.wert=wert;
    }

    public String getMedikament() {
        return medikament;
    }

    public void setMedikament(String medikament) {
        this.medikament = medikament;
    }

    public String getWert() {
        return wert;
    }

    public void setWert(String wert) {
        this.wert = wert;
    }
}
