package com.tuv001.medizinplatform;
/**
 * Created by JTJ-PC
 */
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.Locale;

public class Feedback extends AppCompatActivity {

    private CheckBox feedbackyes;
    private CheckBox feedbackno;
    private Button sendfeedback;
    private String enteredfeedback;
    private String username;
    //Spinner
    private Spinner rating;
    private int enteredrating = 0;// rating value entered by the user

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        //Recovering from SharedPreferences
        SharedPreferences prefs = getSharedPreferences("Mypreferences", Context.MODE_PRIVATE);
        final String UsernameShared=prefs.getString("username","forexampleusername");

        //Spinner
        rating = (Spinner) findViewById(R.id.spinner02);

        rating.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1:
                        enteredrating = position;
                        break;
                    case 2:
                        enteredrating = position;
                        break;
                    case 3:
                        enteredrating = position;
                        break;
                    case 4:
                        enteredrating = position;
                        break;
                    case 5:
                        enteredrating = position;
                        break;
                    case 6:
                        enteredrating = position;
                        break;
                    case 7:
                        enteredrating = position;
                        break;
                    case 8:
                        enteredrating = position;
                        break;
                    case 9:
                        enteredrating = position;
                        break;
                    case 10:
                        enteredrating = position;
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //CheckBox and Button
        feedbackno = (CheckBox) findViewById(R.id.feedbackno);
        feedbackyes = (CheckBox) findViewById(R.id.feedbackyes);
        sendfeedback = (Button) findViewById(R.id.sendFeedback);

        sendfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent;
                intent= new Intent(Feedback.this, PatientMain.class);
                //Multilanguage Strings
                String messagethanks=getResources().getString(R.string.messagethankscooperation);
                String messageyourvalues=getResources().getString(R.string.messageYourvalues);
                String messagerating=getResources().getString(R.string.messageratingscale);
                String messagechooseonlyone=getResources().getString(R.string.messageChooseonlyone);
                String messageand=getResources().getString(R.string.y);

                if (feedbackno.isChecked() && feedbackyes.isChecked())
                {
                    Toast.makeText(Feedback.this, messagechooseonlyone, Toast.LENGTH_SHORT).show();
                }
                else if (feedbackno.isChecked() && enteredrating != 0)
                {
                    enteredfeedback = "Nein";

                    /*
                    DB Query
                    */

                    Toast.makeText(Feedback.this, messagethanks+" "+UsernameShared+"\n"+messageyourvalues+" "
                            +gettranslation(enteredfeedback)+" "+messageand+" "+messagerating+" "+enteredrating, Toast.LENGTH_SHORT).show();

                    startActivity(intent);

                }
                else if (feedbackyes.isChecked() && enteredrating != 0)
                {
                    enteredfeedback = "Ja";

                     /*
                    DB Query
                    */


                    Toast.makeText(Feedback.this, messagethanks+" "+UsernameShared+"\n"+messageyourvalues
                                    +" "+gettranslation(enteredfeedback)+" "+messageand+" "+messagerating+" "+enteredrating, Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                }
            }
        });
    }

   /*
     DB Query
      */


    //Method that translates feedback from German into Spanish or English
    private String gettranslation(String enteredfeedback) {

        //Get the current language
        String language= Locale.getDefault().getLanguage();

        if(language.equals("es"))
        {
            if(enteredfeedback.equals("Ja"))
            {
                enteredfeedback="Si";
            }
            else{
                enteredfeedback="No";
            }
        }
        else if(language.equals("en"))
        {
           if(enteredfeedback.equals("Ja")){
               enteredfeedback="Yes";
           }
           else{
               enteredfeedback="No";
           }
        }
        return enteredfeedback;
    }
}


