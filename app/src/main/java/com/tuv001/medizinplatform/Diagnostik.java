package com.tuv001.medizinplatform;

/**
 * Created by JTJ-PC on 02.05.2017.
 */

public class Diagnostik
{
    String disease;
    String medicine;

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public Diagnostik(String disease, String medicine) {

        this.disease = disease;
        this.medicine = medicine;
    }
}
