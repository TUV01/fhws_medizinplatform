package com.tuv001.medizinplatform;
/**
 * Created by JTJ
 */
//GUI (JTJ)
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import java.util.ArrayList;
//DB (Magommed)
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


    public class Medikament extends AppCompatActivity {

        //Bewertung
        private BewertungAdapter adapter; //Custom Adapter for Object Bewertung
        private ListView listView;
        private ArrayList<Bewertung> bewertungList;
        private String Kranheitselected; ////To save the disease value selected by the user.

        //Magommed from *
        Integer[] Bewertung1 = new Integer[5];
        String[] medikament = new String[5];
        //to *

        //Spinner
        private Spinner listoption;
        private String []optionSpinner={"Liste der Krankheiten","Lebensmittelvergiftung","Grippe","Erkältung","Migräne","Gicht",
                "Blinddarmentzündung","Mandelentzündung", "Heuschnupfen","Lungenentzündung","Mittelohrentzündung"};

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_medikament);

            //Bewertung
            listView=(ListView)findViewById(R.id.listview);

            //Spinner
            listoption=(Spinner)findViewById(R.id.spinner01);

            listoption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position){
                        case 1:
                            setWidgetB(position);
                            break;
                        case 2:
                            setWidgetB(position);
                            break;
                        case 3:
                            setWidgetB(position);
                            break;
                        case 4:
                            setWidgetB(position);
                            break;
                        case 5:
                            setWidgetB(position);
                            break;
                        case 6:
                            setWidgetB(position);
                            break;
                        case 7:
                            setWidgetB(position);
                            break;
                        case 8:
                            setWidgetB(position);
                            break;
                        case 9:
                            setWidgetB(position);
                            break;
                        case 10:
                            setWidgetB(position);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        //Method setWidgetB created by JTJ
        private void setWidgetB(int position)
        {
            String Krankheitselected=optionSpinner[position]; //for example position=1 Krankheitselected="Lebensmittelvergiftung"

            getMedikamentBW(Krankheitselected); //DB Method

            //Adding data to ArrayList
            bewertungList=new ArrayList<Bewertung>();

            //Magommed from *
            bewertungList.add(new Bewertung(medikament[0], Bewertung1[0].toString()));
            bewertungList.add(new Bewertung(medikament[1], Bewertung1[1].toString()));
            bewertungList.add(new Bewertung(medikament[2], Bewertung1[2].toString()));
            bewertungList.add(new Bewertung(medikament[3], Bewertung1[3].toString()));
            bewertungList.add(new Bewertung(medikament[4], Bewertung1[4].toString()));
            //to *

            //Setup Adapter for Listview
            adapter = new BewertungAdapter(Medikament.this,bewertungList);
            listView.setAdapter(adapter);
        }


        //Method getMedikamentBw created by Magommed
        private void getMedikamentBW(String krankheit) {
            boolean isSuccess;
            Connection con;
            String z = "";

            int i = 1;
            try {
                ConnectionDB cDB = new ConnectionDB();
                con = cDB.connectDB();   // Connect to database
                if (con == null) {
                    z = "Check Your Internet Access!";
                } else {
                    String query = "select Medikament, Bewertung from Medikamente where Krankheit = '" + krankheit.toString() + "'  ";
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(query);
                    while (rs.next()) {
                        medikament[i-1] = rs.getString("Medikament");
                        Bewertung1[i-1] = rs.getInt("Bewertung");
                        z = "Medicine received";
                        i++;
                    }
                }
            } catch (Exception ex) {

                z = ex.getMessage();
            }
        }




    }


